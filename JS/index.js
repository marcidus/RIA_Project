const canvas = document.getElementById('soviet27');
        //création de la fenêtre de Canvas
const ctx = canvas.getContext("2d"),
    width = window.innerWidth,
    height = window.innerHeight,
    ratio = window.devicePixelRatio;
let w = 1024;
let h = 480;
let min = 120;
let score = 0;
let level = 2;
let gameFrame = 0;
let gameOver = false;
let live = 3;
ctx.font = '50px Georgia';
//hauteur et largeur du canvas
//canvas.height = window.innerHeight;
//canvas.width = window.innerWidth;
canvas.height = 670;
canvas.width = 1024;
canvas.style.width = width + "px";
canvas.style.height = height + "px";
    //arrow interactivity
const arrow = {
    x: 100,
    y: canvas.height/2,
    click: false
};
//la classe joueur
//création de l'image du joueur
const joueurImg =new Image();
joueurImg.src = '../Images/Soviet.png';
//création des vies
const poupe1 = new Image();
poupe1.src = '../Images/poupe1.png';
const poupe2 = new Image();
poupe2.src = '../Images/poupe2.png';
const poupe3 = new Image();
poupe3.src = '../Images/poupe3.png';
class Player{
        //le constructeur
    constructor(){
        this.position = {
            x: 100,
            y: height/2
        };
        this.width = 100;
        this.height = 120;
        this.life = 3;
    }
        //les méthodes
            //update le joueur dans la position actuelle
    update(){
        const dx = 0; /*comparer la distance entre le joueur et la cible*/
        const dy = this.position.y - arrow.y;
        if(arrow.y != this.position.y){
            this.position.y -= dy/10; /*on règle la vitesse d'arrêt*/
        }
    }
            //dessiner le joueurs
    draw(){
                ctx.drawImage(joueurImg,
                            this.position.x,
                            this.position.y,
                            this.width,
                            this.height
                             );
                if(this.life ==3){
                    ctx.drawImage(poupe1,
                                 700,
                                 30
                                 );
                    ctx.drawImage(poupe2,
                                 720,
                                 20
                                 );
                    ctx.drawImage(poupe3,
                                 745,
                                 10);
                }else if(this.life ==2){
                    ctx.drawImage(poupe1,
                                 700,
                                 30
                                 );
                    ctx.drawImage(poupe2,
                                 720,
                                 20);
                }else if(this.life ==1){
                    ctx.drawImage(poupe1,
                                 700,
                                 30);
                }
    }
    handleHealth(){
        player1.life -= 1;
    }
}
    //affichage du joueur
const player1 = new Player(20, 50);
//les réfugiés/traitres
//création de l'image du garçon
const garcon1 =new Image();
garcon1.src = '../Images/garcon1.png';
const garcon2 =new Image();
garcon2.src = '../Images/garcon2.png';
//création de l'image femme
const femme1 = new Image();
femme1.src = '../Images/femme1.png';
const femme2 = new Image();
femme2.src = '../Images/femme2.png';
//création bouteille
const bouteille = new Image();
bouteille.src = '../Images/bouteille.png';
//création du vieux
const vieux1 = new Image();
vieux1.src = '../Images/vieux1.png';
const vieux2 = new Image();
vieux2.src = '../Images/vieux2.png';
const bottlesArray = [];
class Bottle{
    constructor(ran){
        this.x = canvas.width + 100 + Math.random() * canvas.width;
        this.y = Math.random() *530;
        this.speed = Math.random()*5+1;
        this.width = 40;
        this.height = 80;
    }
    update(){
        this.x -= this.speed;
    }
    drawBottle(){
        if(gameFrame % 2 >= 0){
            ctx.drawImage(bouteille,
        this.x,
        this.y,
        this.width,
        this.height
        );}
    }
}
const refugeesArray = [];
class Refugee{
    //constructeur
    constructor(ran){
        this.x = canvas.width + 100 + Math.random() * canvas.width; /*pour le nombre qui spawn*/
        this.y = Math.random() *530;
        this.ran = Math.random()*3;
        this.speed = Math.random()*5+1;
        this.width = 100;
        this.height = 100;
        this.counted = false;
        this.type = null;
    }
    //méthode
    update(){
        this.x -= this.speed;
        const dy = this.y - player1.position.y;
        const dx = this.x - player1.position.x;
    }
    drawGarcon(){
    this.type = "garcon";
    if(gameFrame % 2 >= 0){
        ctx.drawImage(garcon2,
        this.x,
        this.y,
        this.width,
        this.height
        );}
    }
    drawVieux(){
    this.type = "vieux";
    if(gameFrame % 2 >= 0){
        ctx.drawImage(vieux2,
        this.x,
        this.y,
        this.width,
        this.height
        );}
    }
    drawFemme(){
        this.type = "femme";
    if(gameFrame % 2 >= 0){
        ctx.drawImage(femme2,
        this.x,
        this.y,
        this.width,
        this.height
        );}
    }
        /*ctx.fillStyle = 'blue';
        ctx.beginPath();
        ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2);
        ctx.fill();
        ctx.closePath();
        ctx.stroke();*/
}
//audio capture
const enemyCapture = document.createElement('audio');
enemyCapture.src = '../Musique/y2meta.mp3';
enemyCapture.playbackRate=2;

const vodkaSound = document.createElement("audio");
const vieuxSound = document.createElement("audio");
const femmeSound = document.createElement("audio");
const yeetSound = document.createElement("audio");

vodkaSound.src = '../Musique/vodka.mp3';
vieuxSound.src = '../Musique/vieux.mp3';
femmeSound.src = '../Musique/sound_women.mp3';
yeetSound.src = '../Musique/yeet.mp3';


    //les fonctions des refugees
function handleRefugees(){
    if(gameFrame%50 == 0 ){
        refugeesArray.push(new Refugee());
    }
    for (let i = 0; i < refugeesArray.length/2; i++){
        //efface le réfugié si il touche le bord #laFrontiere
        if(refugeesArray[i].x < 0 ){
            refugeesArray.splice(i, 1);
            yeetSound.play();
            player1.handleHealth();
        }
        if(refugeesArray[i].y < 60){
            refugeesArray.splice(i, 1);
        }
        //gère les collisions entre le garde et le réfugié
        if(collision(refugeesArray[i].x, refugeesArray[i].y, refugeesArray[i].width, refugeesArray[i].height, player1.position.x, player1.position.y, player1.width, player1.height)){
            if(refugeesArray[i].type == "garcon"){
               enemyCapture.play();
            }else if(refugeesArray[i].type == "femme"){
                femmeSound.play();
            }else if(refugeesArray[i].type == "vieux"){
                vieuxSound.play();
            }

            if(!refugeesArray[i].counted){
                score++;
                refugeesArray[i].counted = true;
                refugeesArray.splice(i, 1);
            }
        }
        //enlève un coeur par réfugié qui touche le bord
        refugeesArray[i].update();
        if(refugeesArray[i].ran <= 1){
            refugeesArray[i].drawGarcon();
        }else if(refugeesArray[i].ran <= 2){
            refugeesArray[i].drawFemme();
        }else if(refugeesArray[i].ran <= 3){
            refugeesArray[i].drawVieux();
        }
    }
}
//les fonctions de la bouteille
function handleBottle(){
    // ajoute une bouteille toutes les 300 frames si les vies sont en dessous de 3
    if(gameFrame%300 == 0 && player1.life<3){
        bottlesArray.push(new Bottle());
    }
    for (let i = 0; i < bottlesArray.length; i++){
        if(bottlesArray[i].x < 0 ){
            bottlesArray.splice(i, 1);
            continue;
        }
        // check collision entre bouteille et joueur
        if(collision(bottlesArray[i].x, bottlesArray[i].y, bottlesArray[i].width, bottlesArray[i].height, player1.position.x, player1.position.y, player1.width, player1.height)){
            if(player1.life<3){
                player1.life++;
                vodkaSound.play();
            }
            bottlesArray.splice(i, 1); // remove la bouteille de l'array
            continue;
        }
        bottlesArray[i].update();
        bottlesArray[i].drawBottle();
    }
}
//les eventsListeners
function updown(event){
                if (event.keyCode == 38 && player1.position.y >= min) // up arrow
                {
                    const up = 75;
                     arrow.y -= up;
                }
                 if (event.keyCode == 40 && player1.position.y <=h) // down arrow
                {
                    const down = 75;
                        arrow.y += down;
                }
                event.preventDefault(); //prevent de faire un page up ou page down
            }

        //bouger le joueur avec le clavier
        addEventListener("keydown", updown);
        //resize la fenêtre
        addEventListener('resize', function(){
            let canvasPosition = canvas.getBoundingClientRect();
        });
        addEventListener('click', function(evt){
            if(gameOver == true){
                console.log('clicked');
            }
        });
        addEventListener('click', function(){
           if(gameOver==true){
               window.location.href = "http://dellea.savingberset.ch/HomePage.html";
           }
        });
        //la fonction
function collision(rectX1, rectY1, rectWidth1, rectHeight1, rectX2, rectY2, rectWidth2, rectHeight2){
    let result = false;
    if(rectX1 < rectX2 + rectWidth2 &&
            rectX1 + rectWidth1 > rectX2 &&
            rectY1 < rectY2 + rectHeight2 &&
            rectY1 + rectHeight1 > rectY2){
        result = true;
    }
    return result;
}
//fond d'écran et musique
const background = new Image();
background.src = '../Images/berlin1.PNG';
const wall = new Image();
wall.src = '../Images/wall.png';
const berlinFall = document.createElement('audio');
berlinFall.src = '../Musique/Piste4.mp3';
berlinFall.volume = 0.1;
function handleBackground(){
    if(level == 2){
        ctx.drawImage(background,0,60,canvas.width,canvas.height);
        berlinFall.play();
    }else if(level == 1){
        ctx.drawImage(wall,0,0,canvas.width,canvas.height);
    }
}
const endGame = document.createElement('audio');
endGame.src = '../Musique/endGame.mp3';
function handleGameOver() {
    if(level == 2){
        level -=1;
        player1.life = 0;
    }else if(level == 1){
        gameOver = true;
        ctx.clearRect(0,0,canvas.width,canvas.height);
        ctx.drawImage(wall,0,0,canvas.width,canvas.height);
        ctx.fillText('GAME OVER, You go directly to Goulag. Your score is: '+ score, 200, 200);
        berlinFall.volume = 0;
        endGame.play();
    }
}

let c = document.getElementById(".beginGame");
        //Boucle d'animation
function animate(){

    ctx.clearRect(0,0,canvas.width,canvas.height);
    handleBackground();
    handleRefugees();
    handleBottle();
    player1.update();
    player1.draw();
    ctx.fillStyle = 'black';
    ctx.fillText('SCORE: ' + score, 80, 50);
    gameFrame++;


    if(player1.life == 0){
        handleGameOver();
    }
    if(gameOver==false) {
        requestAnimationFrame(animate);
    }
}




    animate();

